<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

//test//
$routes->get('/test', 'Home::index');

//Multiple User(guest, user, admin)

$routes->get('/about', 'Home::about');
$routes->get('/informasi-pulau', 'Post::show');
$routes->get('/informasi-pulau/(:segment)', 'Post::tampil/$1');

$routes->get('/map', 'Home::show');

//Role User

$routes->get('/', 'User::index');
$routes->get('/profile', 'User::index');
$routes->get('/edit-profile/(:any)(:num)', 'User::profile/$1/$1');
$routes->put('edit-profile/(:any)', 'User::update/$1');
$routes->get('/form-p4t', 'Form::index');
$routes->get('/bukti-register/(:any)', 'Form::eksport/$1');
$routes->put('/form-p4t', 'Form::insert');


//Role Admin

$routes->get('/admin', 'Admin::index', ['filter' => 'role:admin']);
$routes->get('/admin/index', 'Admin::index', ['filter' => 'role:admin']);
$routes->get('/admin/(:num)', 'Admin::detail/$1', ['filter' => 'role:admin']);
$routes->get('/form-verifikasi', 'Form::show', ['filter' => 'role:admin']);
$routes->put('form-verifikasi/(:any)', 'Form::update/$1', ['filter' => 'role:admin']);
$routes->get('/posts', 'Post::index', ['filter' => 'role:admin']);
$routes->get('/posts/add-post', 'Post::tampilkan', ['filter' => 'role:admin']);
$routes->get('/post/(:segment)', 'Post::detail/$1');
$routes->get('/post-delete/(:segment)', 'Post::delete/$1');
$routes->put('/posts/add-post', 'Post::insert', ['filter' => 'role:admin']);
$routes->get('/add-admin', 'Admin::show', ['filter' => 'role:admin']);
$routes->put('/add-admin', 'Admin::insert', ['filter' => 'role:admin']);

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
