<?php

namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model
{
    protected $table      = 'auth_groups_users';
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['group_id', 'user_id'];
}