<?php

namespace App\Models;

use CodeIgniter\Model;

class FormModel extends Model
{
    protected $table          = 'form-p4t';
    protected $useTimestamps   = true;
    protected $allowedFields  = [
        'fullname', 'ktp', 'kk', 'skt','p4t', 'status', 'no-registrasi', 'created_at'
    ];

    public function generateCode($date){
        $builder = $this->table('form-p4t');
        $builder->selectMax('no_registrasi', 'no_registrasiMAX');
        $query = $builder->get();

        if ($query->getNumRows()>0) {
            foreach ($query->getResult() as $key) {
                $kd = '';
                $ambildata = substr($key->no_registrasiMAX, 1);
                $increment = intval($ambildata)+1;
                $kd = sprintf('%04s', $increment);
            }
        }else{
            $kd = '0001';
        }

        return $kd.'/Pengajuan/P4T/BPN/'.$date;
    }

    public function getPagination($num ,$keyword, $nomor)
    {
        if($keyword != ''){
            $builder = $this->table('form-p4t');
            $builder->like('status', $keyword);
            $builder->orLike('no_registrasi', $keyword);
            $query = $builder->get();
        }

        $nomor = nomor($nomor, 10);
        $result = $query->getResult();
        $form = $this->paginate($num);
        $pager = $this->pager;

        $data = [
            'form' => $form,
            'pager' => $pager,
            'result' => $result,
            'i' =>  $nomor,
        ];
        
        return $data;
    }
}