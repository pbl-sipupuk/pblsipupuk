<?php
namespace App\Models;

use CodeIgniter\Model;

class PostsModel extends Model
{
    protected $table          = 'posts';
    protected $allowedFields  = [
        'judul', 'post_image', 'creator', 'slug', 'user_image', 'post',
    ];
  
    
}