<?= $this->extend('templates/export');?>

<?php $this->Section('eksport')?>

<div class="register">
    <div class="page">
        
        <div class="subpage">
            <div class="kop">
            <img src="<?= base_url()?>/images/bpn.png">
            <div class="kop-1">
            <div class="bold"><b>KEMENTRIAN AGRARIA DAN TATA RUANG/ BADAN PERTANAHAN NASIONAL
            <div class="kantor">KANTOR PERTANAHAN KOTA BATAM PROVINSI KEPULAUAN RIAU</div></b>
            <div class="jalan">
                <p class="jln">Jalan Jaksa Agung R. Soeprapto, Sekupang&emsp;Telp : 0778 322643&emsp;Email : kot-batam@atrbpn.go.id</p>
            </div>    
            </div>
            </div>
        </div>
          <p class="judul"><b> BUKTI REGISTRASI PENGAJUAN P4T </b></p>
          <div class="container-isi">
            <table cellpadding="20">
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?= user()->fullname?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><?= user()->email?></td>
                </tr>
                <tr>
                    <td>Nomor Registrasi</td>
                    <td>:</td>
                    <td><?= $form['no_registrasi']?></td>
                </tr>
            </table>
          </div>
          <div class="container">
            <p class="ket"><b>Perhatikan&emsp;:</b></p>
            <p class="isi">
                <ol style="list-style-type: decimal;">
                    <li>Surat ini sebagai tanda bukti pengajuan Formulir P4T Badan Pertanahan Nasional Provinsi Kepulauan Riau tahun <?= date('Y')?>.</li>
                    <li>Selanjutnya silahkan menunggu proses verifikasi oleh petugas.</li>
                    <li>Informasi terkini terkait pengajuan Formulir P4T dapat dipantau melalui website SiPupuk Badan Pertanahan Nasional Provinsi Kepulauan Riau.</li>
                </ol>
            </p>
          </div>
          
          <div class="footer">
            <div class="content">
            <p>
                Batam, <?= $tanggal ?>
          </p>
          <div class="content-center">
          <p>
            Kepala Dinas Pertanahan
          </p>
          <p>Kota Batam</p>
          <p class="image">
            <img src="" alt="">
          </p>
          
          <p>NURZALIE, AP. S.Sos</p>
            <p class="nip">NIP : 197302061993111001</p>
          </div>
          
            </div>
          </div>
        </div>    
    </div>
</div>

<?php $this->endSection()?>