<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="row">
    <div class="col-lg-8">
    <h1 class="h3 mb-4 text-gray-800">Formulir P4T</h1>
    </div>
    <div class="col-lg-4">
         <a href="<?= base_url()?>/doc/form-SKT.doc" class="btn btn-warning text-decoration-none text-white d-block" >Unduh Formulir</a>
    </div>
    </div>
    <div class="card" style="overflow: auto;">
  <div class="card-body">
  <div class="card">
  <div class="card-body">
    <small style="color: gray;">*Formulir Surat Kepemilikan Tanah dan formulir P4T dapat di unduh dan harus memiliki tanda tangan Kepala desa maupun Lurah setempat</small>
    <small style="color: red;"><p>*Semua File Document harus diupload dengan format (.pdf)</p></small>
  </div>
</div>
<h5 style="font-weight: bolder;margin-top: 2rem;margin-bottom: 2rem; text-align: center;">Formulir Pengajuan Penguasaan, Pemilikan, Penggunaan dan Pemanfaatan Tanah (P4T)</h5>
 <!-- Button trigger modal -->
<div class="row">
  <div class="col-lg-8">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formModal">
  Ajukan Form
</button>
  </div>
  <!-- <div class="col-lg-4" style="padding: 10px; align-self: center;">
   <form action="" method="get" autocomplete="off">
    <input type="text" name="search" value="" class="form-control" style="width: 100%;" placeholder="Search....">
    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
   </form>
  </div> -->
</div>

  <table class="table table-striped text-center" style="font-size: 13px; margin-top: 2rem;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">KTP</th>
      <th scope="col">KK</th>
      <th scope="col">Surat Keterangan Kepemilikan Tanah</th>
      <th scope="col">Surat P4T</th>
      <th scope="col">Status</th>
      <th scope="col">Keterangan</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <?php 
      foreach($form as $f) : ?>
        <td><?= $i++ ?></td>
        <td><?= $f['fullname'] ?></td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalktp<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;KTP</span></a>
        </td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalkk<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;KK</span></a>
        </td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalskt<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;SKT</span></a>
        </td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalp4t<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;P4T</span></a>
        </td>
        <td>
          <?php if ($f['status'] === 'On Process'):?>
            <span class="badge bg-warning text-white" style="font-size:12px;"><?= $f['status'] ?></span>
          <?php elseif ($f['status'] === 'Verified'):?>
            <span class="badge bg-success text-white" style="font-size:12px;"><?= $f['status'] ?></span>
          <?php elseif ($f['status'] === 'Rejected'):?>
            <span class="badge bg-danger text-white" style="font-size:12px;"><?= $f['status'] ?></span>
          <?php endif;?>
        </td>
        <td>
          <a type="button" style="font-size:12px; background-color: #1A374D;" class="badge text-decoration-none text-white" value="pdf" onclick="window.open('<?= site_url('/bukti-register/' . $f['id'])?>', 'blank')">Cetak Registrasi</a>
        </td>
    </tr>
  </tbody>
  
<!-- Modal view pdf-->
<div class="modal fade" id="formModalktp<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview KTP</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" style="width: 100%; height:100vh;">
       <embed id="ktp" src="<?= base_url() ?>/doc/<?= $f['ktp']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="formModalkk<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview KK</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" style="width: 100%; height:100vh;">
       <embed src="<?= base_url(); ?>/doc/<?= $f['kk']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="formModalskt<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview Surat Keterangan Tanah (SKT)</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" style="width: 100%; height:100vh;">
       <embed src="<?= base_url(); ?>/doc/<?= $f['skt']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="formModalp4t<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" >
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview P4T</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body"style="width: 100%; height:100vh;">
       <embed src="<?= base_url(); ?>/doc/<?= $f['p4t']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>
  <?php endforeach; ?>
</table>
<?= $pager->links('default', 'pagination') ?>
  </div>
</div>
<!-- Modal form-->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Form Pengajuan P4T</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <span style="margin-bottom: 2rem;"><small style="color: red;">Semua File yang di upload harus berupa file pdf</small></span> 
        <form action="<?= site_url('form-p4t/') ?>" method="post" enctype="multipart/form-data">
        <?= csrf_field() ?>
      <input type="hidden" name="_method" value="PUT">
      <div class="form-group">
        <input type="hidden" value="<?= user()->fullname?>" name="fullname" id="fullname" aria-hidden="true">
        <input type="hidden" value="<?= date('Y')?>" name="date" id="date" aria-hidden="true">
      </div>
      <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="file" class="d-block" name="ktp">
        </div>
    </div>
    <label for="ktp">Upload KTP</label>
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="file" class="d-block" name="kk"  >
        </div>
    </div>
    <label for="email-label">Upload KK</label>
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="file" class="d-block" name="skt" >
        </div>
    </div>
    <label for="email-label">Upload SKT</label>
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="file" class="d-block" name="p4t">
        </div>
    </div>
    <label for="email-label">Upload Form P4T</label>
  </fieldset>
  <div class="form-group">
    <input type="hidden" name="status" id="status" aria-hidden="true" value="On Process">
  </div>
    </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>
</div>

<?= $this->endSection();?>