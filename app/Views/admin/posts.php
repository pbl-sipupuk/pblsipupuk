<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="row">
    <div class="col-lg-10">
    <h1 class="h3 mb-4 text-gray-800">Posts Informasi Pulau</h1>
    </div>
    </div>
    <div class="card" style="overflow: auto;">
  <div class="card-body">
<a type="button" class="btn btn-primary" href="<?= base_url('posts/add-post')?>">
  <i class="fa fa-plus"></i>&emsp;Add Posts
</a>

  <table class="table table-striped text-center" style="font-size: 13px; margin-top: 2rem;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col"></th>
      <th scope="col">Title</th>
      <th scope="col"></th>
      <th scope="col">Creator</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    <?php 
    foreach($post as $p): ?>
      <td><?= $i++?></td>
      <?php if ($p['post_image']): ?>
      <td><img src="<?= base_url() ?>/uploads/berkas/<?= $p['post_image']?>" alt="<?= $p['post_image']?>" style="width:150px; height: 80px; object-fit: cover;"></td>
      <?php else : ?>
        <td></td>
      <?php endif; ?>
      <td><?= $p['judul']?></td>
      <td><img src="<?= base_url()?>/img/<?= $p['user_image'] ?>" alt="profile" class="img-profile rounded-circle" style="width:50px; height: 50px; object-fit: cover;"></td>
      <td><?= $p['creator']?></td>
      <td> <a href="<?= base_url('/post/'. $p['slug']);?>" class="btn btn-info" style="text-decoration: none;">
        <i class="fas fa-eye"></i>&emsp;Detail</td>
    </tr>
  </tbody>
  <?php endforeach; ?>
</div>
</table>
  <?= $pager->links('default', 'pagination') ?>
  </div>
</div>
</div>


<?= $this->endSection();?>