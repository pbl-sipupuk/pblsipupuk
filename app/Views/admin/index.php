<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">User List</h1>

    <div class="card" style="overflow: auto;">
  <div class="card-body">
  <table class="table table-striped text-center" style="font-size: 13px;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col"></th>
      <th scope="col">Username</th>
      <th scope="col">Email</th>
      <th scope="col">Role</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1 ?>
    <?php foreach($users as $user): ?>

    <tr>
      <th scope="row"><?= $i++ ?></th>
      <td><img src="<?= base_url();?>/img/<?= $user->user_image ?>" alt="profile picture" style="object-fit: cover; width: 50px; height:50px; border-radius: 50% ;"></td>
      <td><?= $user->username ?></td>
      <td><?= $user->email ?></td>
      <td><?= $user->name ?></td>
      <td>
        <a href="<?= base_url('admin/' .$user->userid);?>" class="btn btn-info" style="text-decoration: none;">
        <i class="fas fa-eye"></i>&emsp;Detail
    </a>
      </td>
    </tr>

    <?php endforeach; ?>

  </tbody>
</table>
  </div>
</div>
</div>
<?= $this->endSection();?>