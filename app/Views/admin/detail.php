<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">User Detail</h1>
    <div class="row" style="margin-top: 30px;">
    <div class="col-lg-12">
    <div class="card mb-3" >
    <div class="row g-0">
    <div class="col-md-4">
      <img src="<?= base_url('/img/' . $users->user_image); ?>" class="img-profile rounded-circle" alt="<?= base_url($users->username); ?>" style="overflow: hidden; object-fit: cover; width: 300px; height:300px;">
    </div>
    <div class="col-md-8">
      <div class="card-body">
      <ul class="list-group list-group-flush">
    <li class="list-group-item"><h4><?=$users->username?></h4></li>
    <?php if ($users->fullname): ?>
    <li class="list-group-item"><h2><?=$users->fullname?></h2></li>
    <?php endif; ?>
    <li class="list-group-item"><h6><?=$users->email?></h6></li>
    <li class="list-group-item text-white"><span class="badge bg-<?= ($users->name == 'admin') ? 'success' : 'warning' ?>"><?=$users->name?></span></li>
    <li class="list-group-item"><a class="btn btn-secondary text-decoration-none" href="<?= base_url('/admin')?>">&laquo; Back to User List</a></li>
    </ul>
    </div>
    </div>
    </div>
     </div>
</div>
</div>
</div>
<?= $this->endSection();?>