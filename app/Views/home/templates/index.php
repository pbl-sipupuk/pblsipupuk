<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SiPupuk | <?= $title ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url()?>/css/main.css">
    <link rel="stylesheet" href="<?= base_url()?>/css/style.css">
    <link rel="manifest" href="<?= base_url()?>/manifest.json"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Swipper CSS -->
    <link rel="stylesheet" href="<?= base_url()?>/css/swiper-bundle.min.css">
  </head>
  <body>
     <?= $this->include('home/templates/navbar');?>
   <?= $this->renderSection('main'); ?>
   <?= $this->include('home/templates/footer');?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
  <!-- Swipper JS -->
  <script src="<?= base_url()?>/js/swiper-bundle.min.js"></script>

  <!-- Java Script -->
  <script src="<?= base_url()?>/js/script.js"></script>
  <script src="<?= base_url()?>/js/script-vertical.js"></script>
  <script>
            document.addEventListener('DOMContentLoaded', init, false);

            function init() {
                if ('serviceWorker' in navigator && navigator.onLine) {
                    navigator.serviceWorker.register('<?= base_url() ?>/service-worker.js')
                    .then((reg) => {
                        console.log('Registrasi service worker Berhasil', reg);
                    }, (err) => {
                        console.error('Registrasi service worker Gagal', err);
                    });
                }
            }
</script>
</html>