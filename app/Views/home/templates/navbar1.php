<nav class="navbar navbar-expand-lg" >
    <div class="container-fluid" style="background-color: #1A374D; z-index: 5; min-width: 100%;">
      <a class="navbar-brand" href="/"><img src="../images/logo(1).png" alt="logo" width="180"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?= base_url('/homepage')?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url('/about')?>">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('/form-p4t')?>">Form Pengajuan P4T</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?= base_url('/informasi-pulau')?>">Informasi Pulau</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('/homepage')?>">Jelajahi</a>
          </li>
        </ul>
        <ul class="navbar-nav ms-auto">
            <li class="nav-item">
                <a href="<?=base_url('/')?>" type="button" class="btn btn-outline-primary">Login</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>