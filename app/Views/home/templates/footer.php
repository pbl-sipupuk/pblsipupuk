    <!-- Footer -->
    <footer>
        <p><b>Sipupuk&emsp;</b><span style="color: grey; border-left: 1px solid white;">&emsp;ⓒ2022 Copy Right</span></p>
        <div>
            <i class="fa fa-facebook"></i>
            <i class="fa fa-instagram"></i>
            <i class="fa fa-twitter"></i>
            <i class="fa fa-linkedin"></i>
        </div>
    </footer>
    <!-- Footer -->