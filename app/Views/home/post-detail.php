<?= $this->extend('home/templates/index1') ?>

<?= $this->Section('main1')?>
<div class="container-card">
<div class="card">
    <div class="row pt-5 ">
    <div class="col-lg-12">
    <h1 class="h3 mb-4 text-gray-800"><?= $post['judul'] ?></h1>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-8 pt-2 pl-4" style="padding: 0 0 2rem 0;">By &ensp;
            <img src="<?= base_url() ?>/img/<?= $post['user_image']?>" alt="creator" class="img-profile rounded-circle" style="width: 30px; height: 30px; object-fit:cover;">
            &ensp;<?= $post['creator']?>
        </div>
        <div class="col-lg-4 pt-2 pr-4 tanggal" style="text-align: right;">
            Diperbaharui <?= $post['created_at']?>
        </div>
    </div>
    <img src="<?= base_url() ?>/img/<?= $post['post_image']?>" alt="<?= $post['post_image']?>" style="object-fit: scale-down;">
    <small class="text-center"><?= $post['post_image']?></small>
    <div class="card-body pt-3" style="padding: 0 20px;">
    <div class="card-text"><?= $post['post']?></div>
  </div>
    </div>
</div>

<?= $this->endSection();?>
