<?php

namespace App\Controllers;

use Myth\Auth\Password;
use Myth\Auth\Models\UserModel;
use App\Controllers\BaseController;

class Admin extends BaseController
{
    public $db, $builder, $user, $hash;

    public function __construct()
    {
      $this->user = new UserModel();
      $this->hash = new Password();
        $this->db      = \Config\Database::connect();
        $this->builder = $this->db->table('users');
    }
    public function index()
    {
       $data['title'] = 'User List'; 

    $this->builder->select('users.id as userid, user_image ,username, email, name');
    $this->builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
    $this->builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
    $query = $this->builder->get();

    $data['users'] = $query->getResult();

       return view('admin/index', $data);
    }

    public function detail($id)
    {
       $data['title'] = 'User Detail'; 

    $this->builder->select('users.id as userid, user_image , username, fullname, email, name');
    $this->builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
    $this->builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
    $this->builder->where('users.id', $id);
    $query = $this->builder->get();

    $data['users'] = $query->getRow();

    if (empty($data['users'])) {
        return redirect()->to('/admin');
    }

       return view('admin/detail', $data);
    }

    public function insert(){
      !$this->validate([
         'username' => [
             'rules' => 'required|min_length[4]|max_length[20]|is_unique[users.username]',
             'errors' => [
                 'required' => '{field} Harus diisi',
                 'min_length' => '{field} Minimal 4 Karakter',
                 'max_length' => '{field} Maksimal 20 Karakter',
                 'is_unique' => 'Username sudah digunakan sebelumnya'
             ]
         ],
         'email' => [
             'rules' => 'required|min_length[4]|max_length[20]|is_unique[users.email]',
             'errors' => [
                 'required' => '{field} Harus diisi',
                 'min_length' => '{field} Minimal 4 Karakter',
                 'max_length' => '{field} Maksimal 20 Karakter',
                 'is_unique' => 'Email sudah digunakan sebelumnya'
             ]
         ],
         'password' => [
             'rules' => 'required|min_length[4]|max_length[50]',
             'errors' => [
                 'required' => '{field} Harus diisi',
                 'min_length' => '{field} Minimal 4 Karakter',
                 'max_length' => '{field} Maksimal 50 Karakter',
             ]
         ],

         'password_conf' => [
             'rules' => 'matches[password]',
             'errors' => [
                 'matches' => 'Konfirmasi Password tidak sesuai dengan password',
             ]
         ],
         'fullname' => [
             'rules' => 'required|min_length[4]|max_length[100]',
             'errors' => [
                 'required' => '{field} Harus diisi',
                 'min_length' => '{field} Minimal 4 Karakter',
                 'max_length' => '{field} Maksimal 100 Karakter',
             ]
         ],
      ]);

      $password = $this->request->getVar('password');
      $active = '1';

      $password_hash = $this->hash->hash($password);

      $data = [
         'email' => $this->request->getVar('email'),
         'username' => $this->request->getVar('username'),
         'fullname' => $this->request->getVar('fullname'),
         'password_hash' => $password_hash,
         'active' => $active
      ];
      $this->user->withGroup('admin')->insert($data);

      return redirect()->to('admin');
    }

    public function show(){
      $data = [
         'title' => 'Add Admin'
      ];

      return view('admin/add-admin', $data);
    }

}