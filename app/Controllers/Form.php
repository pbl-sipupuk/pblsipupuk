<?php

namespace App\Controllers;

use App\Models\FormModel;
use App\Controllers\BaseController;
use \Dompdf\Dompdf;

class Form extends BaseController
{
    public $db, $builder;
    protected $formModel, $dompdf;

    public function __construct()
    {
        $this->dompdf = new Dompdf();
        $this->formModel = new FormModel();
        $this->db      = \Config\Database::connect();
        $this->builder = $this->db->table('form-p4t');
    }

    public function index()
    {
        $form = $this->formModel->paginate(10);
        $pager = $this->formModel->pager;
        $nomor = nomor($this->request->getVar('page'), 10);

        $data = [
            'title' => 'Pengisian Formulir P4T',
            'form' => $form,
            'pager' => $pager,
            'i' =>  $nomor,
        ] ;

           return view('user/form-p4t', $data);
    }
    
    public function show()
    {
        $form = $this->formModel->paginate(10);
        $pager = $this->formModel->pager;
        $nomor = nomor($this->request->getVar('page'), 10);

        $data = [
            'title' => 'Pengisian Formulir P4T',
            'form' => $form,
            'pager' => $pager,
            'i' => $nomor,
        ] ;

           return view('admin/form-verifikasi', $data);
    }
    
    public function eksport($id)
    {
        $form = $this->formModel->where(['id' => $id])->first(); 

        $tanggal = date('Y-m-d');
        switch (date('m', strtotime($tanggal))) {
            case '01':
                $bulan = 'Januari';
                break;
            case '02':
                $bulan = 'Februari';
                break;
            case '03':
                $bulan = 'Maret';
                break;
            case '04':
                $bulan = 'April';
                break;
            case '05':
                $bulan = 'Mei';
                break;
            case '06':
                $bulan = 'Juni';
                break;
            case '07':
                $bulan = 'Juli';
                break;
            case '08':
                $bulan = 'Agustus';
                break;
            case '09':
                $bulan = 'September';
                break;
            case '10':
                $bulan = 'Oktober';
                break;
            case '11':
                $bulan = 'November';
                break;
            case '12':
                $bulan = 'Desember';
                break;
            
            default:
                $bulan = 'Tidak diketahui';
                break;
        }

        $date = date('d', strtotime($tanggal)).' '. $bulan .' '. date('Y', strtotime($tanggal));

        $data = [
            'title' => 'Bukti Registrasi',
            'tanggal' => $date,
            'form' => $form,
        ];

        return view('user/export', $data);
     }
    
    public function insert()
    {
        $this->validate([
            'ktp' => 'uploaded[ktp]|max_size[ktp,300]'
                           . '|mime_in[ktp,pdf,]'
                           . '|ext_in[userfile,pdf]',
            'kk' => 'uploaded[kk]|max_size[kk,300]'
                           . '|mime_in[kk,pdf]'
                           . '|ext_in[kk,pdf]',
            'skt' => 'uploaded[skt]|max_size[skt,300]'
                           . '|mime_in[skt,pdf]'
                           . '|ext_in[skt,pdf]',
            'p4t' => 'uploaded[p4t]|max_size[p4t,300]'
                           . '|mime_in[p4t,pdf]'
                           . '|ext_in[p4t,pdf]',
        ]);

        $fullname = $this->request->getVar('fullname');
        $status = $this->request->getVar('status');
        $date = $this->request->getVar('date');
        $kode = $this->formModel->generateCode($date);

        $filektp = $this->request->getFile('ktp');
        $filektp->move('doc');
        $ktpname = $filektp->getName();

        $filekk = $this->request->getFile('kk');
        $filekk->move('doc');
        $kkname = $filekk->getName();

        $fileskt = $this->request->getFile('skt');
        $fileskt->move('doc');
        $sktname = $fileskt->getName();

        $filep4t = $this->request->getFile('p4t');
        $filep4t->move('doc');
        $p4tname = $filektp->getName();

        $this->builder->set('fullname', $fullname);
        $this->builder->set('ktp', $ktpname);
        $this->builder->set('kk', $kkname);
        $this->builder->set('skt', $sktname);
        $this->builder->set('p4t', $p4tname);
        $this->builder->set('status', $status);
        $this->builder->set('created_at', $date);
        $this->builder->set('no_registrasi', $kode);
        $this->builder->insert();
        $query = $this->builder->get();

        $data['form-p4t'] = $query->getResult();

        if (empty($data['form-p4t'])) {
            return redirect()->to('/form-p4t');
        }

        return redirect()->to('/form-p4t');
     }

     public function update($id){

        $status = $this->request->getVar('status');
        $this->builder->set('status', $status);
        $this->builder->where('id', $id)->update();
        $query = $this->builder->get();

        $data['form-p4t'] = $query->getRow();

        if (empty($data['form-p4t'])) {
            return redirect()->to('/form-verifikasi/');
        }

        return redirect()->to('/form-verifikasi');
     }
}
