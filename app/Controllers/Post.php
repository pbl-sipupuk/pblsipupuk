<?php

namespace App\Controllers;

use App\Models\PostsModel;
use App\Controllers\BaseController;

class Post extends BaseController
{
    public $db, $builder;
    protected $postsModel;

    public function __construct()
    {
        $this->postModel = new PostsModel();
        $this->db      = \Config\Database::connect();
        $this->builder = $this->db->table('posts');
    }

    public function index()
    {
        $post = $this->postModel->paginate(10);
        $pager = $this->postModel->pager;
        $nomor = nomor($this->request->getVar('page'), 10);

        $data = [
            'title' => 'Post Informasi Pulau',
            'post' => $post,
            'pager' => $pager,
            'i' => $nomor,
        ] ;

           return view('admin/posts', $data);
    }
    public function tampilkan()
    {
        
        $post = $this->postModel->findAll();

        $data = [
            'title' => 'Tambah Informasi Pulau',
            'post' => $post,
        ] ;

           return view('admin/add-post', $data);
    }

    public function show()
    {
        $post = $this->postModel->findAll();
        $data = [
            'title' => 'Informasi Pulau',
            'post' => $post,
        ] ;

           return view('home/posts', $data);
    }

    public function detail($slug){
        $post = $this->postModel->where(['slug' => $slug])->first();

        $data = [
            'title' => 'Post Informasi Pulau',
            'post' => $post,
        ] ;

        return view('admin/post-detail', $data);
    }

    public function insert()
    {
        $this->validate([
            'post_image' => 'uploaded[post_image]|max_size[post_image,100]'
                            . '|mime_in[post_image,image/png,image/jpg,image/gif]'
                            . '|ext_in[post_image,png,jpg,gif]|max_dims[post_image,1024,768]',
        ]);

        $judul = $this->request->getVar('judul');
        $date = $this->request->getVar('date');
        $creator = $this->request->getVar('fullname');
        $post_area = $this->request->getVar('post_area');
        $posting = explode("\r\n\r\n", $post_area);
        $text = "";

        for ($i=0; $i <=count($posting)-1 ; $i++) { 
            $part = str_replace($posting[$i], "<p>".$posting[$i]."</p>", $posting[$i]);
            $text .= $part;
        }
        $user_image = $this->request->getVar('user_image');
        $slug = url_title($this->request->getVar('judul'), '-', true);

        $file = $this->request->getFile('post');
        $file->move('uploads/berkas');
        $imagename = $file->getName();

        $this->builder->set('judul', $judul);
        $this->builder->set('creator', $creator);
        $this->builder->set('post', $text);
        $this->builder->set('user_image', $user_image);
        $this->builder->set('slug', $slug);
        $this->builder->set('post_image', $imagename);
        $this->builder->set('created_at', $date);
        $this->builder->insert();
        $query = $this->builder->get();

        $data['posts'] = $query->getResult();

        if (empty($data['posts'])) {
            return redirect()->to('posts');
        }

        return redirect()->to('/posts');
    }

    public function tampil($slug){
        $post = $this->postModel->where(['slug' => $slug])->first();

        $data = [
            'title' => 'Informasi Pulau',
            'post' => $post,
        ] ;

        return view('home/post-detail', $data);
    }
    
    public function delete($slug){
        $this->builder->delete(['slug' => $slug]);
        return redirect()->to('/posts');
    }

}
