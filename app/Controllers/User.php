<?php

namespace App\Controllers;

class User extends BaseController
{
    public $db, $builder;

    public function __construct()
    {
        $this->db      = \Config\Database::connect();
        $this->builder = $this->db->table('users');
    }

    public function index()
    {
        $data['title'] = 'Profile';
        return view('user/index', $data);
    }

    public function profile($id)
    {
        $data['title'] = 'Profile';

           return view('user/edit-profile', $data);
        }


    public function update($id){
        $this->validate([
            'userfile' => 'uploaded[userfile]|max_size[userfile,100]'
                           . '|mime_in[userfile,image/png,image/jpg,image/gif]'
                           . '|ext_in[userfile,png,jpg,gif]|max_dims[userfile,1024,768]',
        ]);

        $username = $this->request->getVar('username');
        $fullname = $this->request->getVar('fullname');
        $email = $this->request->getVar('email');
        $file = $this->request->getFile('userfile');
        $file->move('img');
        $filename = $file->getName();

        $this->builder->set('user_image', $filename);
        $this->builder->set('username', $username);
        $this->builder->set('fullname', $fullname);
        $this->builder->set('email', $email);
        $this->builder->where('id', $id)->update();
        $query = $this->builder->get();

        $data['users'] = $query->getRow();

        if (empty($data['users'])) {
            return redirect()->to('/profile');
        }

        return redirect()->to('/profile');
    }
    }