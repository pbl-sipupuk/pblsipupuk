<?php

namespace App\Controllers;

class NotFound extends BaseController
{
    public function index()
    {
        echo "404 PAGE NOT FOUND";
    }
}