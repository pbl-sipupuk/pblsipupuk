<?= $this->extend('home/templates/index1') ?>

<?= $this->Section('main1')?>
<div class="container-luar ">
<div class="slide-container swiper">
    <div class="slide-content ">
        <div class="card-wrapper swiper-wrapper">
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/egi(1).jpg" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Regita Ayu Cahyani</h2>
                    <p class="description">
                        <button class="button">Developer</button>
                    </p>
                </div>
            </div>
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/pram.jpg" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Pramudya Ananta</h2>
                    <p class="description">
                    <button class="button">Developer</button>
                    </p>
                </div>
            </div>
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/Arin.jpg" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Arin Juniarti</h2>
                    <p class="description">
                        <button class="button">Planner</button>
                    </p>
                </div>
            </div>
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/veni.jpg" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Veni Permata Sari</h2>
                    <p class="description">
                        <button class="button">Planner</button>
                    </p>
                </div>
            </div>
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/fajri.png" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Muchamad Fajri A N, S.ST., M.Sc</h2>
                    <p class="description">
                        <button class="button">Manager Project PBL-15 SiPupuk</button>
                    </p>
                </div>
            </div>
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/okta.png" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Oktavianto Gustin, S.T., M.T</h2>
                    <p class="description">
                       <button class="button">Co-Manager Project PBL-15 SiPupuk</button>
                    </p>
                </div>
            </div>
            <div class="card swiper-slide">
                <div class="image-content">
                    <span class="overlay">
                        
                    </span>
                    <div class="card-image">
                    <img src="<?= base_url()?>/img/riwi.png" alt="" class="card-img">
                    </div>
                </div>

                <div class="card-content">
                    <h2 class="name">Riwinoto, S.T., M.Kom</h2>
                    <p class="description">
                        <button class="button">Pengusul Project PBL-15 SiPupuk</button>
                    </p>
                </div>
            </div>
            </div>
        </div>
        
      <div class="container-swipper">
             <div class="swiper-pagination"></div>
      </div>
    </div>
    <div class="swiper-button-next swiper-navBtn"></div>
      <div class="swiper-button-prev swiper-navBtn"></div>
</div>

<?= $this->endSection() ?>