<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title?></title>
    <!-- css  -->
    <link rel="stylesheet" href="<?= base_url()?>/css/email.css">
</head>
<body>
        <div class="container-wrap">
            <div class="logo">
                <img src="<?= base_url()?>/images/logo.png" alt="logo" class="bpn">
                <img src="<?= base_url()?>/images/img-12.png" alt="sipupuk" class="sipupuk">
            </div>
            <div class="wrap">
        <p class="date">
           Batam, <?= date('d M Y || H : i : s')?>
        </p>
        <p>&emsp;Anda dapat mengatur ulang kata sandi untuk halaman web site kami <?= site_url() ?>.</p>
        <p>Untuk dapat mengatur ulang kata sandi, gunakan kode atau URL berikut dan ikuti petunjuknya.</p>
        </div>
        <div class="container-hash">
            <p><u>Your Token</u></p>
        </div>
        <div class="hash">
        <p class="code">jdsgdkhsg</p>
        </div>
        <div class="link">
            <a href="#" class="btn btn-primary">Reset Form</a>
        </div>
        <div class="wrap">
        <br>
        <p>Abaikan pesan ini apabila Anda tidak meminta untuk mengatur ulang kata sandi. Terimakasih</p>
        <p class="date">
           Salam, SiPupuk
        </p>
        </div>
        </div>
</body>
</html>