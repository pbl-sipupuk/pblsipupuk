<?= $this->extend('home/templates/index1') ?>

<?= $this->Section('main1')?>
<div class="container-luar">
   <div class="container-dalam">
    <h1 class="judul"><b>Informasi Pulau</b></h1>
   <div class="row">
    <?php foreach($post as $p):?>
      <?php if($post == null):?>
        <p class="text-center"><b>Maaf Belum Ada Postingan...</b></p>
      <?php endif;?>
    <div class="col-md-4">
    <div class="card">
  <img src="<?= base_url()?>/img/<?= $p['post_image']?>" class="card-img-top" alt="sipupuk">
  <div class="card-body text-center">
    <a href="<?= base_url('/informasi-pulau/'. $p['slug']);?>"><h5 class="card-title"><b><?= $p['judul']?></b></h5></a>
  </div>
  <div class="card-footer">
   <p>By <?= $p['creator']?></p>
   <p><b><a href="<?= base_url('/informasi-pulau/'. $p['slug']);?>">Read More</a></b></p>
</div>
</div>
    </div>
    <?php endforeach;?>
</div>
</div>
</div>
<?= $this->endSection(); ?>