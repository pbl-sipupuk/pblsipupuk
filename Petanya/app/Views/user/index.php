<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-10">
        <h1 class="h3 mb-4 text-gray-800">Profile</h1>
        </div>
        <div class="col-lg-2">
            <a href="<?= base_url('/edit-profile/' . user()->username . user()->id);?>" class="btn text-decoration-none text-white d-block" style="background-color: #1A374D;">Edit Profile</a>
        </div>
    </div>
    <div class="row" style="margin-top: 30px;">
    <div class="col-lg-12">
    <div class="card mb-3" >
    <div class="row g-0">
    <div class="col-md-4 text-center">
    <img src="<?= base_url('/img/' . user()->user_image); ?>" alt="<?= base_url(user()->username); ?>" class="img-profile rounded-circle" style=" overflow: hidden; width:300px; height:300px; object-fit: cover;">
    </div>
    <div class="col-md-8">
      <div class="card-body">
      <ul class="list-group list-group-flush">
    <li class="list-group-item"><h4><?=user()->username?></h4></li>
    <?php if (user()->fullname): ?>
    <li class="list-group-item"><h2><?=user()->fullname?></h2></li>
    <?php endif; ?>
    <li class="list-group-item"><h6><?=user()->email?></h6></li>
    </ul>
    </div>
    </div>
    </div>
     </div>
</div>
</div>
</div>
<?= $this->endSection();?>