<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Profile</h1>
    <div class="row" style="margin-top: 30px;">
    <div class="col-lg-12">
    <div class="card mb-3"  >    
      <form action="<?= site_url('edit-profile/' . user()->id ) ?>" method="post" enctype="multipart/form-data">
      <?= csrf_field() ?>
      <input type="hidden" name="_method" value="PUT">
    <div class="row g-0">
      <div class="col-md-4 text-center">
      <img src="<?= base_url('/img/' . user()->user_image); ?>" alt="<?= base_url(user()->username); ?>" class="img-profile rounded-circle" style=" overflow: hidden; width:300px; height:300px; object-fit: cover;">
      <input class="d-block" type="file" name="userfile" id="profile" style="margin: 1rem 4.5rem;">
    </div>
    <div class="col-md-8">
      <div class="card-body">
      <ul class="list-group list-group-flush">
    <li class="list-group-item"><input type="text" name="username" value="<?= user()->username?>" class="form-control d-block" style="width: 100%; border: none;"></li>
    <?php if (user()->fullname): ?>
    <li class="list-group-item"><input type="text" name="fullname" placeholder="Name" value="<?= user()->fullname?>"  class="form-control d-block" style="width: 100%; border: none;"></li>
    <?php elseif(user()->fullname === null):?>
      <li class="list-group-item"><input type="text" name="fullname" placeholder="Full Name" class="form-control d-block" style="width: 100%; border: none;"></li> 
    <?php endif; ?>
    <li class="list-group-item"><input type="email" name="email" value="<?= user()->email?>" class="form-control d-block" style="width: 100%; border: none;"></li>
    </ul>
    </div>
    </div>
    <div class="col-lg-12 pt-5 pb-5">
    <button class="btn text-white d-block" style="background-color: #1A374D; width: 90%; margin: 0 auto 0 auto;" type="submit">Save</button>
    </div>
    </form>
    </div>
     </div>
</div>
</div>
</div>
<?= $this->endSection();?>