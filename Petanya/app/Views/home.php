<?= $this->extend('home/templates/index') ?>

<?= $this->Section('main') ?>
<section class="wrap" id="home">
    <div>
        <h1><b>
                <p>Jelajahi Pulau </p>
                <p>Lebih Mudah</p>
            </b></h1>
        <p>
        <h5><b>SiPupuk</b> memberikan kemudahan dan
            pengalaman menarik yang dapat anda rasakan
            untuk jelajahi pulau dengan sekali klik</h5>
        </p>
    </div>
    <img src=" http://localhost/pgwl3/petalokasiobjek/public/images/img-06.png" alt="sipupuk">
</section>
<section class="wrap-container" id="form">
    <div>
        <h1><b>
                <p>Data P4T</p>
            </b></h1>
        <p>
        <h5><b>Berisi data Penguasaan, Pemilikan, Penggunaan dan pemanfaatan tanah (P4T) </b>hadir secara online
            Memudahkan Anda untuk melihat data kepemilikan tanah di pulau-pulau kecil wilayah Kepulauan Riau</h5>
        </p>
        <div>
            <a href="<?= site_url('objek/peta_polygon') ?>" class="btn btn-outline-primary"><b>Lihat Peta</b></a>
        </div>
    </div>
    <img src="http://localhost/pgwl3/petalokasiobjek/public/images/p4t.gif" alt="sipupuk">
</section>
<section class="content-container" id="informasi">
    <section class="sec-1">
        <div class="card">
            <img src="http://localhost/pgwl3/petalokasiobjek/public/img/pulau-abang.jpg" alt="sipupuk">
            <div class="card-body">
                <h4><b>Pulau Abang</b></h4>
            </div>
            <div class="card-footer">
                <p>By Creator</p>
                <p><b>Read More</b></p>
            </div>
        </div>
        <div class="card">
            <img src="http://localhost/pgwl3/petalokasiobjek/public/img/ranoh.jpg" alt="sipupuk">
            <div class="card-body">
                <h4><b>Pulau Ranoh</b></h4>
            </div>
            <div class="card-footer">
                <p>By Creator</p>
                <p><b>Read More</b></p>
            </div>
        </div>
        <div class="card">
            <img src="http://localhost/pgwl3/petalokasiobjek/public/img/petong.jpg" alt="sipupuk">
            <div class="card-body">
                <h4><b>Pulau Petong</b></h4>
            </div>
            <div class="card-footer">
                <p>By Creator</p>
                <p><b>Read More</b></p>
            </div>
        </div>
    </section>
    <section class="sec-2">
        <div>
            <h1><b>
                    <p>Informasi Pulau Kecil</p>
                </b></h1>
            <p>
            <h5>Anda dapat mengakses informasi pulau-pulau kecil sekitar Batam terkait<b> Informasi Umum, Fasum, Data Kepemilikan Tanah dan Tempat Wisata</h5>
            </p>
            <div>
                <a href="<?= site_url('objek/view') ?>" class="btn btn-outline-primary"><b>Lihat Peta</b></a>
            </div>
    </section>
</section>
<section class="end-content" id="jelajahi">
    <div>
        <img src="http://localhost/pgwl3/petalokasiobjek/public/images/img-12.png" alt="sipupuk" style="width: 90px;">
        <div>
            <h1><b>
                    <p>Coba SiPupuk Sekarang</p>
                </b></h1>
            <p>
            <h5 style="color: gray;"><b>Mulai Gratis</b></p>
                <p>Nikmati pengalaman baru untuk <span style="color: #1A374D;"><b>Jelajahi Pulau</b></span></p>
            </h5>
            <div>
                <a href="#" class="btn btn-outline-primary"><b>Jelajahi</b></a>
            </div>
</section>
<?= $this->endSection(); ?>