<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon" >
        <img src="<?= base_url(); ?>/images/img-01.png" alt="sipupuk.com"style="width: 5vw">
    </div>
    <div class="sidebar-brand-text mx-3">SiPupuk</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('/homepage');?>">
        <i class="fas fa-fw fa-home"></i>
        <span>Home</span></a>
</li>
<?php if(in_groups('admin')):?>
    <!-- Nav Item - Dashboard -->
<!-- <li class="nav-item">
    <a class="nav-link" href="tables.html">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li> -->
<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('/posts/');?>">
        <i class="fas fa-fw fa-newspaper"></i>
        <span>Post</span></a>
</li>

<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin');?>">
        <i class="fas fa-fw fa-users"></i>
        <span>User List</span></a>
</li>

<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('/add-admin');?>">
        <i class="fas fa-fw fa-user"></i>
        <span>Add Admin Account</span></a>
</li>
<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('/form-verifikasi/');?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Form Verification</span></a>
</li>

<!-- Nav Item - Charts -->
<li class="nav-item">
    <a class="nav-link" href="charts.html">
        <i class="fas fa-fw fa-map"></i>
        <span>Add Maps</span></a>
</li>

<?php elseif(in_groups('user')):?>
    <!-- Nav Item - Dashboard -->
<!-- <li class="nav-item">
    <a class="nav-link" href="">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li> -->
<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('/informasi-pulau/');?>">
        <i class="fas fa-fw fa-newspaper"></i>
        <span>Informasi Pulau</span></a>
</li>

<!-- Nav Item --->
<li class="nav-item">
        <a class="nav-link" href="<?= base_url('/form-p4t/');?>">
        <i class="fas fa-fw fa-table"></i>
        <span>Formulir P4T</span></a>
</li>

<!-- Nav Item - Charts -->
<li class="nav-item">
    <a class="nav-link" href="charts.html">
        <i class="fas fa-fw fa-map"></i>
        <span>Maps</span></a>
</li>
<?php endif;?>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>