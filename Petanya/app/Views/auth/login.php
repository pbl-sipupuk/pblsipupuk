<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SiPupuk | Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url(); ?>/css/login.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div class="limiter">
    <div class="container-landing">
            <div class="wrap-login100">
             <div class="login100-pic">
                 <img src="<?= base_url();?>/images/img-05.png" alt="IMG">
             </div>

             <form class="login100-form validate-form" action="<?= url_to('login') ?>" method="post">
				<?= csrf_field() ?>
        
                <span class="login100-form-logo">
                    <a href="<?= url_to('homepage');?>">
                     <img src="<?= base_url();?>/images/Logo.png">	
                    </a>
                </span>
                <?= view('Myth\Auth\Views\_message_block') ?>
                 <span class="login100-form-title" style="text-transform: uppercase;letter-spacing: 8px;">
                 <?=lang('Auth.loginTitle')?>
                 </span>
 <?php if ($config->validFields): ?>
                 <div class="wrap-input100 validate-input">
                     <input type="email" class="input100 form-control <?php if (session('errors.login')) : ?>is-invalid<?php endif ?>"
								   name="login" placeholder="<?=lang('Auth.email')?>">
                     <span class="focus-input100"></span>
                     <span class="symbol-input100">
                         <i class="fa fa-envelope" aria-hidden="true"></i>
                     </span>
                 </div>
                 <div class="invalid-feedback">
								<?= session('errors.login') ?>
					 </div>
 <?php endif; ?>
                 <div class="wrap-input100 validate-input" data-validate = "Password is required">
                 <input type="password" name="password" class="input100 form-control  <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.password')?>">
                     <span class="focus-input100"></span>
                     <span class="symbol-input100">
                         <i class="fa fa-lock" aria-hidden="true"></i>
                     </span>
                     </div>
                     <div class="invalid-feedback">
						<?= session('errors.password') ?>
						</div>
 <?php if ($config->activeResetter): ?>
                 <div class="text">
                     <span class="txt1">
                         Forgot
                     </span>
                     <a class="txt2" href="<?= url_to('forgot') ?>">
                          Password?
                     </a>
                 </div>
 <?php endif; ?>
 <?php if ($config->allowRemembering): ?>
                 <div class="form-group">
							<label class="form-check-label">
								<input type="checkbox" name="remember" class="form-check-input" <?php if (old('remember')) : ?> checked <?php endif ?>>
								<?=lang('Auth.rememberMe')?>
							</label>
						</div>
 <?php endif; ?>
                 <div class="container-login100-form-btn">
                     <button class="btn login100-form-btn btn-block" type="submit"><?=lang('Auth.loginAction')?></button>
                 </div>
 <?php if ($config->allowRegistration) : ?>       
                 <div class="text1 text-center">
                     <a class="txt3" href="<?= url_to('register') ?>">
                         Create your Account
                         <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                     </a>
                 </div>
 <?php endif; ?>
             </form>
            </div>
         </div>
    </div>
</body>
</html>