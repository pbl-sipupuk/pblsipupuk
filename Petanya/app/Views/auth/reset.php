<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SiPupuk | Reset Password</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url(); ?>/css/login.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div class="limiter">
    <div class="container-landing">
             <div class="wrap-login103">
             <form class="login100-form validate-form" action="<?= url_to('reset-password') ?>" method="post">
				<?= csrf_field() ?>
         <span class="login100-form-logo">
             <a href="<?= url_to('homepage');?>">
             <img src="<?= base_url()?>/images/Logo.png">	
             </a>
         </span>

         <span class="login100-form-title">
            <p><?=lang('Auth.forgotPassword')?></p>
         </span>

         <?= view('Myth\Auth\Views\_message_block') ?>

         <div class="wrap-input100 validate-input">
             <input class="input100 form-control <?php if (session('errors.token')) : ?>is-invalid<?php endif ?>" type="text" name="token" placeholder="<?=lang('Auth.token')?>">
             <span class="focus-input100"></span>
             <span class="symbol-input100">
                 <i class="fa fa-gear" aria-hidden="true"></i>
             </span>
         </div>
         <div class="invalid-feedback"><?= session('errors.token') ?></div>

         <div class="wrap-input100 validate-input">
             <input class="input100 form-control <?php if (session('errors.email')) : ?>is-invalid<?php endif ?>" type="email" name="email" placeholder="<?=lang('Auth.email')?>"
                aria-describedby="emailHelp" value="<?= old('email') ?>">
             <span class="focus-input100"></span>
             <span class="symbol-input100">
                 <i class="fa fa-envelope" aria-hidden="true"></i>
             </span>
         </div>
         <div class="invalid-feedback"><?= session('errors.email') ?></div>

         <div class="wrap-input100 validate-input">
             <input class="input100 form-control <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>" type="password" name="password" placeholder="<?=lang('Auth.newPassword')?>">
             <span class="focus-input100"></span>
             <span class="symbol-input100">
                 <i class="fa fa-lock" aria-hidden="true"></i>
             </span>
         </div>
         <div class="invalid-feedback"><?= session('errors.password') ?></div>

         <div class="wrap-input100 validate-input">
             <input class="input100 form-control <?php if (session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" type="password" name="pass_confirm" placeholder="<?=lang('Auth.newPasswordRepeat')?>">
             <span class="focus-input100"></span>
             <span class="symbol-input100">
                 <i class="fa fa-lock" aria-hidden="true"></i>
             </span>
         </div>
         <div class="invalid-feedback"><?= session('errors.pass_confirm') ?></div>

         <div class="container-login100-form-btn">
             <button class="login100-form-btn" type="submit">
                 <i class="fa fa-trash"></i>&emsp;<?=lang('Auth.resetPassword')?>
             </button>
         </div>

     </form>
    </div>
</div>

    </div>
</body>
</html>