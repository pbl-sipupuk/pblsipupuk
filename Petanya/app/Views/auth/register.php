<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SiPupuk | Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url(); ?>/css/login.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<div class="limiter">
    <div class="container-landing">
	<div class="wrap-login101">
		<div class="login101-pic">
			<img src="<?= base_url(); ?>/images/img-09.png" alt="IMG">
		</div>
		
		<form class="login100-form validate-form" action="<?= url_to('register') ?>" method="post">
            <?= csrf_field() ?>

			<span class="login100-form-logo">
				<a href="<?= base_url('/homepage');?>">
				<img src="<?= base_url(); ?>/images/Logo.png">	
				</a>
			</span>
			<span class="login100-form-title" style="text-transform: uppercase; letter-spacing: 8px;">
			<?=lang('Auth.register')?>
			</span>
			<?= view('Myth\Auth\Views\_message_block') ?>

			<div class="wrap-input100">
			<input type="email" class="input100 form-control <?php if (session('errors.email')) : ?>is-invalid<?php endif ?>" name="email" aria-describedby="emailHelp" placeholder="<?=lang('Auth.email')?>" value="<?= old('email') ?>">
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100">
			<input type="text" class="input100 form-control <?php if (session('errors.username')) : ?>is-invalid<?php endif ?>" name="username" placeholder="<?=lang('Auth.username')?>" value="<?= old('username') ?>">
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-user" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100">
			<input type="password" name="password" class="input100 form-control <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.password')?>" autocomplete="off">
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-lock" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100">
			<input type="password" name="pass_confirm" class="input100 form-control <?php if (session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.repeatPassword')?>" autocomplete="off">
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-lock" aria-hidden="true"></i>
				</span>
			</div>
			
			<div class="container-login100-form-btn pb-50">
				<button type="submit" class="btn login100-form-btn btn-block"><?=lang('Auth.register')?></button>
			</div>	

			<div class="text1 text-center">
				<a class="txt3" href="<?= url_to('login') ?>">
					Have an Account ?
					<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
				</a>
			</div>
		</form>
	</div>
</div>
</div>
</body>
</html>