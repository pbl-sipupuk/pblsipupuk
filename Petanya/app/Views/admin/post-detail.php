<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
<div class="row">
    <div class="col-lg-8">
    <h1 class="h3 mb-4 text-gray-800"><?= $post['judul'] ?></h1>
    </div>
    <div class="col-lg-4 d-block" style="text-align: right;">
        <a href="<?= base_url('/post-delete/' . $post['slug'])?>" class="btn btn-danger text-white" style="width: 100%;">Delete</a>
    </div>
    </div>
    <div class="card" style="padding: 2rem 2rem 0 2rem;">
    <div class="row">
        <div class="col-lg-8 pt-2 pl-4" style="padding: 0 0 2rem 0;">By &ensp;
            <img src="<?= base_url() ?>/img/<?= $post['user_image']?>" alt="creator" class="img-profile rounded-circle" style="width: 30px; height: 30px; object-fit:cover;">
            &ensp;<?= $post['creator']?>
        </div>
        <div class="col-lg-4 pt-2 pr-4" style="text-align: right;">
            Diperbaharui <?= $post['created_at']?>
        </div>
    </div>
    <img src="<?= base_url() ?>/img/<?= $post['post_image']?>" alt="<?= $post['post_image']?>" style="object-fit: scale-down;">
    <small class="text-center"><?= $post['post_image']?></small>
    <div class="card-body pt-5">
    <div class="card-text"><?= $post['post']?></div>
  </div>
    </div>
</div>
<?= $this->endSection();?>