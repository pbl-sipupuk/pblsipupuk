<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="row">
    <div class="col-lg-10">
    <h1 class="h3 mb-4 text-gray-800">Add Informasi Pulau</h1>
    </div>
    </div>
    <div class="card" style="overflow: auto;">
  <div class="card-body">
      <form action="<?= site_url('/posts/add-post') ?>" method="post" enctype="multipart/form-data">
        <?= csrf_field() ?>
      <input type="hidden" name="_method" value="PUT">
      <div class="form-group">
        <input type="hidden" value="<?= user()->fullname?>" name="fullname" id="fullname" aria-hidden="true">
        <input type="hidden" value="<?= user()->user_image?>" name="user_image" id="user_image" aria-hidden="true">
        <input type="hidden" value="<?= date('Y-m-d')?>" name="date" aria-hidden="true">
      </div>
      <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="file" class="d-block" name="post" >
        </div>
    </div>
    <label for="email-label">Upload Header Postingan</label>
  </fieldset>
      <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="text" class="form-control"  placeholder="Judul"  name="judul" style="width:100%;">
        </div>
    </div>
    <label for="ktp">Title</label>
  </fieldset>
  <fieldset class="form-group">
        <textarea type="text" class="form-control" placeholder="Isi postingan disini..." name="post_area" id="post" rows="15" style="width:100%; "></textarea>
      </fieldset>
      <d class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
</d>
      </form>
  </div>
    </div>
</div>
<?= $this->endSection();?>