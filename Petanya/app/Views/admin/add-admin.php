<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-10">
    <h1 class="h3 mb-4 text-gray-800">Add Admin Account</h1>
    </div>
    </div>
    <div class="card"  style="overflow: auto;">
        <div class="card-body">
        <form action="<?= base_url('/add-admin')?>" method="POST">
        <?= csrf_field() ?>
      <input type="hidden" name="_method" value="PUT">
      <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="text" class="form-control" name="username">
        </div>
    </div>
    <label for="ktp">Username</label>
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="text" class="form-control" name="fullname"  >
        </div>
    </div>
    <label for="email-label">Nama Lengkap</label>
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="text" class="form-control" name="email" >
        </div>
    </div>
    <label for="email-label">E-mail</label>
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="password" class="form-control" name="password" id="inputPassword">
        <input type="checkbox" onclick="myFunction()" style="margin: 1.5rem 1rem;"><span style="font-size: 14px;">Show Password</span>
        </div>
    </div>
    <label for="email-label">Password</label>
    
  </fieldset>
  <fieldset class="form-group">
    <div class="card">
        <div class="card-body">
        <input type="password" class="form-control" name="password_conf">
        </div>
    </div>
    <label for="email-label">Konfirmasi Password</label>
    
  </fieldset>
    </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
        </form>
        </div>
    </div>

    
    <script>
        function myFunction() {
            var x = document.getElementById("inputPassword");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
<?= $this->endSection()?>