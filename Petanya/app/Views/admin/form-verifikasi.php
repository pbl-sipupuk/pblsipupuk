<?= $this->extend('templates/index');?>

<?= $this->Section('page-content'); ?>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="row">
    <div class="col-lg-12">
    <h1 class="h3 mb-4 text-gray-800">Form Verification</h1>
    </div>
    </div>
    <div class="card" style="overflow: auto;">
  <div class="card-body">
  
<h5 style="font-weight: bolder;margin-top: 2rem;margin-bottom: 2rem; text-align: center;">Formulir Verifikasi Penguasaan, Pemilikan, Penggunaan dan Pemanfaatan Tanah (P4T)</h5>

  <table class="table table-striped text-center" style="font-size: 13px; margin-top: 2rem;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pengaju</th>
      <th scope="col">KTP</th>
      <th scope="col">KK</th>
      <th scope="col">Surat Keterangan Kepemilikan Tanah</th>
      <th scope="col">Surat P4T</th>
      <th scope="col">Status</th>
      <th scope="col">No Registrasi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <?php foreach($form as $f) : ?>
        <td><?= $i++ ?></td>
        <td><?= $f['fullname'] ?></td>
        <td>
          <a type="button" style="width: 100px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalktp<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;KTP</span></a>
        </td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalkk<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;KK</span></a>
        </td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalskt<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;SKT</span></a>
        </td>
        <td>
          <a type="button" style="width: 90px; font-size: 12px;" class="btn btn-info" data-toggle="modal" data-target="#formModalp4t<?=$f['id']?>">
           <i class="fa fa-eye"></i><span class="text">&emsp;P4T</span></a>
        </td>
        <td>
        <form action="<?= site_url('form-verifikasi/' . $f['id'] ) ?>" method="post" >
        <?= csrf_field() ?>
      <input type="hidden" name="_method" value="PUT">
        <?php 
        if ($f['status'] == "On Process") {?>
            <input type="submit" style="width: 90px; font-size: 12px; margin-bottom: 0.5rem;" name="status" value="Verified" class="btn btn-success">
            <input type="submit"  style="width: 90px; font-size: 12px;" name="status" value="Rejected" class="btn btn-danger">
        <?php }else if ($f['status'] == "Verified") {?>
            <span class="badge bg-success text-white text-center" style="font-size:12px;"><?= $f['status']?></span>
        <?php }else{?>
            <span class="badge bg-danger text-white text-center" style="font-size:12px;"><?= $f['status']?></span>
        <?php }
        ?>
    </form>
        </td>
        <td>
          <span class="no-registrasi" style="color: #4169e1;"><b><?= $f['no_registrasi']?></b></span>
        </td>
    </tr>
  </tbody>

<!-- Modal view pdf-->
<div class="modal fade" id="formModalktp<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview KTP</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" style="width: 100%; height:100vh;">
       <embed src="<?= base_url() ?>/doc/<?= $f['ktp']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="formModalkk<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview KK</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" style="width: 100%; height:100vh;">
       <embed src="<?= base_url(); ?>/doc/<?= $f['kk']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="formModalskt<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview Surat Keterangan Tanah (SKT)</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body" style="width: 100%; height:100vh;">
       <embed src="<?= base_url(); ?>/doc/<?= $f['skt']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="formModalp4t<?=$f['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" >
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Preview P4T</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body"style="width: 100%; height:100vh;">
       <embed src="<?= base_url(); ?>/doc/<?= $f['p4t']?>" type="application/pdf" width="100%" height="100%">
    </div>
  </div>
</div>
</div>
  <?php endforeach; ?>
</table>
<?= $pager->links('default', 'pagination') ?>
  </div>
</div>

<!-- Modal form-->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title fs-5" id="exampleModalLabel">Form Pengajuan P4T</h6>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <span style="margin-bottom: 2rem;"><small>Semua File yang di upload harus berupa file pdf</small></span> 
       
    </div>
  </div>
</div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
<?= $this->endSection();?>