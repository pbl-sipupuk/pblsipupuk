<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LeafletJS GeoJSON Point</title>
    <link href="https://unsorry.net/assets-date/images/favicon.png" rel="shortcut icon" type="image/png" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.1/dist/leaflet.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.12/leaflet-routing-machine.css" integrity="sha512-eD3SR/R7bcJ9YJeaUe7KX8u8naADgalpY/oNJ6AHvp1ODHF3iR8V9W4UgU611SD/jI0GsFbijyDBAzSOg+n+iQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        html,
        body,
        #map {
            height: 100%;
            width: 100%;
            margin: 0px;
            overflow: hidden;
        }

        #map {
            width: auto;
            height: calc(100% - 56px);
            margin-top: 56px;
        }
    </style>
</head>

<body>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="https://unpkg.com/leaflet@1.9.1/dist/leaflet.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.12/leaflet-routing-machine.js" integrity="sha512-OcKb9Sl9mxicJ0pARTouh6txFaz3dG1DtFhezkSmZ5CD0PfQ+/XRCwvSkw46a7OSL5TgX35iF1L/zFXGC2tdBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><i class="fas fa-map-marked"></i> BPN KOTA BATAM</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <!-- ref to tabel -->
                    <li class="nav-item">
                        <?php if (auth()->loggedIn()) : ?>
                            <a class="nav-link" href="<?= base_url('objek/table') ?>"> <i class="fas fa-table"></i> Tabel </a>
                    </li>
                <?php endif; ?>

                <!-- input data mechanism -->
                <li class="nav-item">
                    <?php if (auth()->loggedIn()) : ?>
                        <a class="nav-link" href="<?= base_url('objek') ?>"> <i class="fas fa-plus-circle"></i> Input Data </a>
                </li>
            <?php endif; ?>

            <!-- ref to leafletdraw -->
            <li class="nav-item">
                <?php if (auth()->loggedIn()) : ?>
                    <a class="nav-link" href="<?= base_url('leafletdraw/index') ?>"> <i class="fas fa-draw-polygon"></i> leaflet draw </a>
            </li>
        <?php endif; ?>

        <!-- login -->
        <li class="nav-item">
            <a class="nav-link <?= auth()->loggedIn() ? 'text-danger' : '' ?>" href="
						<?= auth()->loggedIn() ? base_url('logout') : base_url('login') ?>
						"><i class="fas fa-sign-in-alt"></i>
                <?= auth()->loggedIn() ? 'Logout' : 'Login' ?>
            </a>
        </li>
        <!-- modal/info -->
        <li class="nav-item">
            <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#infomodal"><i class="fas fa-info-circle"></i> Info </a>
        </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="map"></div>

    <!-- Modal -->
    <div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel"><i class="fas fa-info-circle"></i>IFNO</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Aplikasi buat pemrograman geospasial web lanjut</p>
                    <p><b>YAHAHAHAHAHAYUUUK</b></p>
                    <p><b>Kamu nenya ini aplikasi apa?</b></p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Baik mengerti !</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        var current_position, current_accuracy, myLoc;
        /* Initial Map */
        var map = L.map("map").setView([1.0948955651073942, 104.02857576241514], 10);

        /* Tile Basemap */
        var basemap = L.tileLayer(
            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            }
        );
        basemap.addTo(map);

        /* GeoJSON Point */
        var point = L.geoJson(null, {
            onEachFeature: function(feature, layer) {
                var popupContent = "Nama: " + feature.properties.nama + "<br>" +
                    "Deskripsi: " + feature.properties.deskripsi + "<br>" +
                    '<button type="button" id="prevBtn" onclick="addWayP(' + feature.geometry.coordinates[1] + ',' + feature.geometry.coordinates[0] + ')"> Jadikan Tujuan </button>';
                layer.on({
                    click: function(e) {
                        point.bindPopup(popupContent);
                    },
                    mouseover: function(e) {
                        point.bindTooltip(feature.properties.nama);
                    },
                });
            },
        });
        $.getJSON("http://localhost/pgwl3/petalokasiobjek/public/api", function(data) {
            point.addData(data);
            map.addLayer(point);
        });

        function onLocationFound(e) {
            if (current_position) {
                map.removeLayer(current_position);
                map.removeLayer(current_accuracy);
            }

            var radius = e.accuracy / 2;

            current_position = L.marker(e.latlng).addTo(map)
                .bindPopup("Anda berada sekitar " + radius + " meter dari titik ini.").openPopup();

            current_accuracy = L.circle(e.latlng, radius).addTo(map);

            myLoc = e.latlng;
            routeControl.spliceWaypoints(0, 1, myLoc);
            console.log(myLoc)
            //console.log(routeControl.getWaypoints().length);
        }

        function onLocationError(e) {
            alert(e.message);
        }

        map.on('locationfound', onLocationFound);
        map.on('locationerror', onLocationError);

        function highlightFeature(e) {
            var layer = e.target;
            layer.setStyle({
                weight: 5,
                color: 'black',
                fillColor: 'white',
                fillOpacity: 0.2
            });
            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }
        }
        map.locate({
            setView: false,
            maxZoom: 13
        });

        function addWayP(y, x) {
            var waypoint = new L.Routing.Waypoint();
            waypoint.latLng = {};
            console.log(y);
            console.log(x);

            waypoint.latLng.lat = y; //e.target.feature.;
            waypoint.latLng.lng = x; //item.Lon;

            //console.log(routeControl.getWaypoints().length);

            //routeControl.spliceWaypoints(routeControl.getWaypoints().length + 1, 0, waypoint.latLng);

            var routeArray = new Array();
            routeArray = routeControl.getWaypoints();
            console.log(routeArray[0].latLng);
            console.log(routeArray[1].latLng);


            if (routeControl.getWaypoints().length == 2) {
                if (routeArray[0].latLng == null) {
                    routeControl.spliceWaypoints(0, 1, waypoint.latLng);
                } else if (routeArray[1].latLng == null) {
                    routeControl.spliceWaypoints(1, 1, waypoint.latLng);
                } else {
                    routeControl.spliceWaypoints(routeControl.getWaypoints().length + 1, 0, waypoint.latLng);
                }
            } else {
                routeControl.spliceWaypoints(routeControl.getWaypoints().length + 1, 0, waypoint.latLng);
            }
            map.closePopup();
            //console.log(routeControl.getWaypoints().length);
        }


        var routeControl = L.Routing.control({
                waypoints: [null],
                geocoder: L.Control.Geocoder.nominatim(),
                addWaypoints: true,
                show: true,
                draggableWaypoints: true,
                routeWhileDragging: true,
                reverseWaypoints: true,
                autoToute: true,
                showAlternatives: true,
                altLineOptions: {
                    styles: [{
                            color: 'black',
                            opacity: 0.15,
                            weight: 9
                        },
                        {
                            color: 'white',
                            opacity: 0.8,
                            weight: 6
                        },
                        {
                            color: 'blue',
                            opacity: 0.5,
                            weight: 2
                        }
                    ]
                },
                waypointNameFallback: function(latLng) {
                    function zeroPad(n) {
                        n = Math.round(n);
                        return n < 10 ? '0' + n : n;
                    }

                    function sexagesimal(p, pos, neg) {
                        var n = Math.abs(p),
                            degs = Math.floor(n),
                            mins = (n - degs) * 60,
                            secs = (mins - Math.floor(mins)) * 60,
                            frac = Math.round((secs - Math.floor(secs)) * 100);
                        return (n >= 0 ? pos : neg) + degs + '°' +
                            zeroPad(mins) + '\'' +
                            zeroPad(secs) + '.' + zeroPad(frac) + '"';
                    }
                    return sexagesimal(latLng.lat, 'N', 'S') + ' ' + sexagesimal(latLng.lng, 'E', 'W');
                },
                show: true
            })
            .on('routingerror', function(e) {
                try {
                    map.getCenter();
                } catch (e) {
                    map.fitBounds(L.latLngBounds(routeControl.getWaypoints().map(function(wp) {
                        return wp.latLng;
                    })));
                }
                //handleError(e);
                //console.log(e);
            }).addTo(map);
    </script>
</body>

</html>