<?php

namespace App\Controllers;

use App\Models\TabelObjekModel;
use App\Controllers\BaseController;

class Objek extends BaseController
{

    protected $TabelObjekModel;

    public function __construct()
    {
        $this->TabelObjekModel = new TabelObjekModel();
        $this->data['validation'] = \Config\Services::validation();
    }

    public function index()
    {
        session();
        return view('v_input', $this->data);
    }


    public function simpantambahdata()
    {
        //validasi
        if (!$this->validate([
            'input_nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'kolom Nama harus diisi',

                ]
            ],
            'input_longitude' => [
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'kolom Longitude harus diisi',
                    'numeric' => 'kolom Longitude harus berupa angka.'
                ]
            ],
            'input_latitude' => [
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'kolom Latitude harus diisi',
                    'numeric' => 'kolom Latitude harus berupa angka.'
                ]
            ],
            'input_foto' => [
                'rules' => 'mime_in[input_foto,image/jpg,image/jpeg,image/png]|max_size[input_foto,10000]',
                'errors' => [
                    'mime_in' => 'file yang anda pilih harus berupa gambar',
                    'max_size' => 'Ukuran Foto maksimal 10 mb',

                ]
            ],

        ])) {
            return redirect()->to('objek')->with("message", 'gagal menambahkan data lokasi objek.')->withInput();
        }

        $file_foto = $this->request->getFile('input_foto');

        if ($file_foto->getError() == 4) {
            $nama_foto = NULL;
        } else {
            $foto_dir = 'upload/foto';
            if (!is_dir($foto_dir)) {
                mkdir($foto_dir, 0777, TRUE);
            }
            $nama_foto = 'foto_' . preg_replace('/\s+/', '', $_POST['input_nama']) . '.' .
                $file_foto->getExtension();

            $file_foto->move($foto_dir, $nama_foto);
        }

        $data = [
            'nama' => $_POST['input_nama'],
            'deskripsi' => $_POST['input_deskripsi'],
            'longitude' => $_POST['input_longitude'],
            'latitude' => $_POST['input_latitude'],
            'foto' => $nama_foto,
        ];
        $this->TabelObjekModel->save($data);
        return redirect()->to('objek/table')->with('message', 'Data berhasil ditambahkan');
    }

    public function view()
    {
        return view('v_map');
    }

    public function table()
    {
        $this->db = \Config\Database::connect("");
        $data['objek'] = $this->TabelObjekModel->findAll();


        return view('v_table', $data);
    }

    public function hapus($id)
    {
        $this->TabelObjekModel->delete($id);
        return redirect()->to('objek/table')->with('message', 'Data berhasil dihapus');
    }

    public function edit($id)
    {
        $data['objek'] = $this->TabelObjekModel->find($id);
        return view('v_edit', $data);
    }

    public function simpaneditdata($id)
    {
        session();

        //simpan data
        // $file_foto2 = $this->request->getFile('input_foto');

        // if ($file_foto2->getError() == 4) {
        //     $nama_foto2 = NULL;
        // } else {
        //     $foto_dir = 'upload/foto';
        //     if (!is_dir($foto_dir)) {
        //         mkdir($foto_dir, 0777, TRUE);
        //     }
        //     $nama_foto2 = 'foto_' . preg_replace('/\s+/', '',$_POST['input_nama']) . '.' .
        //     $file_foto2->getExtension();

        //     $file_foto2->move($foto_dir, $nama_foto2);

        $file_foto = $this->request->getFile('input_foto');

        if ($file_foto->getError() == 4) {
            if ($_POST['input_foto_lama'] !== '') {
                $nama_foto = $_POST['input_foto_lama'];
            } else {
                $nama_foto = NULL;
            }
        } else {
            $foto_dir = 'upload/foto';
            if (!is_dir($foto_dir)) {
                mkdir($foto_dir, 0777, TRUE);
            }
            //cek foto lama existing
            if ($_POST['input_foto_lama'] !== '') {
                if (file_exists($foto_dir . $_POST['input_foto_lama'])) {
                    unlink($foto_dir . $_POST['input_foto_lama']);
                }
            }
            $nama_foto = 'foto_' . preg_replace('/\s+/', '', $_POST['input_nama']) . '.' .
                $file_foto->getExtension();



            $file_foto->move($foto_dir, $nama_foto);
        }
        $data = [
            'id' => $id,
            'nama' => $_POST['input_nama'],
            'deskripsi' => $_POST['input_deskripsi'],
            'longitude' => $_POST['input_longitude'],
            'latitude' => $_POST['input_latitude'],
            'foto' => $nama_foto,
        ];
        $this->TabelObjekModel->save($data);
        return redirect()->to('objek/table')->with('message', 'Data berhasil diubah');
    }

    public function home()
    {
        return view('home/homepage');
    }

    public function peta()
    {
        return view('v_map');
    }

    public function peta_polygon()
    {
        return view('leafletdraw/v_map_polygon');
    }
}
