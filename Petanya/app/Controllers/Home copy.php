<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data['title'] = 'test';
        return view('home/coba', $data);
    }
    
    public function about()
    {
        $data['title'] = 'Tentang Kami';
        return view('home/about', $data);
    }
    public function show()
    {
        return view('home/v_map');
    }
    
}
